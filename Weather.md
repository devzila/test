## Endpoint API
**GET**'v1/Weather'
## Parameters
Name   |   Description  |  Required\otional
-------|----------------|------------------
city   |  cityname      |  Required

##Example Request
```shell
curl -X POST \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d '{"city":"Panchkula"}' \
  http://localhost:3000/v1/Weather
```

## Success Response

Status Code - 200

```json
{
  "success": true,
  "message": "Success",
  "data": {
    "city": {
      "cityname": "Panchkula",
      "Temprature": "21",
      "Rain": "0%",
    
      "created_at": "2016-12-27T07:46:41.000Z",
      "updated_at": "2016-12-27T07:46:41.000Z"
    },
    "auth_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxNSwiZXhwIjoxNDgyOTExMjAxfQ.lqOY6UXFJD47aaSIBqJAWheCH27Z8Wlnv9o8DC8oSBQ"
  }
}
```
Status Code - 401

